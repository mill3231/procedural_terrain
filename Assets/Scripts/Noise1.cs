﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Noise1
{
    public static float[,] GenNoiseMap(int mapWidth, int mapHeight, int octaves, float persistance, float lacunarity, int seed, float scale = 0.0001f)
    {
        float[,] noiseMap = new float[mapWidth, mapHeight];

        System.Random prng = new System.Random(seed);
        Vector2[] octaveOffsets = new Vector2[octaves];

        for(int i = 0; i < octaves; i++)
        {
            float offsetX = prng.Next(-100000, 100000);
            float offsetY = prng.Next(-100000, 100000);

            octaveOffsets[i] = new Vector2(offsetX, offsetY);
        }

        float maxNoiseHeight = float.MinValue;
        float minNoiseHeight = float.MaxValue;

        for(int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {

                float amplitude = 1;
                float frequency = 1;
                float noiseHeight = 0;


                for(int i = 0; i < octaves; i++)
                {
                    float sampleX = x / scale * frequency + octaveOffsets[i].x;
                    float sampleY = y / scale * frequency + octaveOffsets[i].y;

                    float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                    noiseHeight += perlinValue * amplitude;

                    amplitude *= persistance;
                    frequency *= lacunarity; 

                }

                //Used to normalize 
                if(noiseHeight > maxNoiseHeight)
                    {
                    maxNoiseHeight = noiseHeight;
                    }
                else if (noiseHeight < minNoiseHeight)
                    {
                    minNoiseHeight = noiseHeight;
                    }
                noiseMap[x, y] = noiseHeight;

            }
        }

            //used to normalize
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    noiseMap[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseMap[x, y]);
                }
            }

       return noiseMap;

    }
    
}
