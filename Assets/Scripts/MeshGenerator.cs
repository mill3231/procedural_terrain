﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class MeshGenerator : MonoBehaviour
{
    Mesh mesh;
    Vector3[] vertices;
    int[] triangles;

    public int xSize = 20;
    public int zSize = 20;
    public float scale = 20;

    // Start is called before the first frame update
    void Start()
    {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        CreateShape();
        UpdateMesh();

    }

   

    void CreateShape()
    {
        vertices = new Vector3[(xSize + 1) * (zSize + 1)];

        for (int i = 0, z = 0; z <= zSize; z++)
        {
            for (int x = 0; x <= xSize; x++)
            {
                float InterimY = GenerateHeights();
                
                //Debug.Log("Height Value: " + InterimY);
                float y = Mathf.PerlinNoise(x * .8f, z * 8f) * 2f;
                //float y = CalulculateHeigth()
                //y *= InterimY;
                vertices[i] = new Vector3(x, y, z);
                i++;
            }
        }

        triangles = new int[xSize * zSize * 6]; //amount of points for each quad is 6
        int vert = 0;
        int tris = 0;

        for (int z = 0; z < zSize; z++)
        {
            for (int x = 0; x < xSize; x++)
            {
                triangles[0 + tris] = vert + 0;
                triangles[1 + tris] = vert + xSize + 1;
                triangles[2 + tris] = vert + 1;
                triangles[3 + tris] = vert + 1;
                triangles[4 + tris] = vert + xSize + 1;
                triangles[5 + tris] = vert + xSize + 2;

                vert++;
                tris += 6;
               
            }

            vert++;
        }

    }



    float GenerateHeights()
    {
        float[,] heights = new float[xSize, zSize];
        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < zSize; y++)
            {
                heights[x, y] = CalulculateHeigth(x, y);
            }
        }
        int select1 = Random.Range(0, 1);
        int select2 = Random.Range(0, 1);
        float val = heights[select1, select2];
        //Debug.Log("Value :" + val);
        return val;
    }



    float CalulculateHeigth(int x, int y)
    {
        float xCoord = (float)x / xSize * scale;
        float yCoord = (float)y / zSize * scale;

        return Mathf.PerlinNoise(xCoord, yCoord);
    }



    void UpdateMesh()
    {
        mesh.Clear();

        mesh.vertices = vertices;
        mesh.triangles = triangles;

        mesh.RecalculateNormals();
    }

}
